@extends('layout.master')
@section('title')
    Halaman Film
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Tambah Data Film
        </div>
        <div class="card-body">
            <form action="/film" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="" class="control-label">Judul</label>
                    <input type="text" class="form-control" name="judul">
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Tahun</label>
                    <input type="number" class="form-control" name="tahun">
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Poster</label>
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="poster">
                            <label class="custom-file-label" for="inputGroupFile02"
                                aria-describedby="inputGroupFileAddon02">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="inputGroupFileAddon02">Upload</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Ringkasan</label>
                    <textarea name="ringkasan" id="" cols="30" rows="10" class="form-control"></textarea>
                </div>

                <button class="btn btn-dark float-right"><i class="fa fa-save"></i> Save</button>
            </form>
        </div>
    </div>
@endsection
