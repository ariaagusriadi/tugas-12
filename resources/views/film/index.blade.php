@extends('layout.master')
@section('title')
    Halaman Film
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Data Film
            @auth
                <a href="film/create" class="btn btn-dark float-right"><i class="fa fa-plus"></i> Tambah Data</a>
            @endauth
        </div>
        <div class="card-body">
            <div class="row">
                @foreach ($film as $item)
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('images/' . $item->poster) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->judul }}</h5><br>
                                <span class="badge badge-primary">{{ $item->genre->nama }}</span>
                                <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
                                @auth
                                    <form action="/film/{{ $item->id }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="/film/{{ $item->id }}" class="btn btn-primary">Detail</a>
                                        <a href="/film/{{ $item->id }}/edit" class="btn btn-primary">Edit</a>
                                        <input type="submit" value="delete" class="btn btn-danger">
                                    </form>
                                @endauth
                                @guest
                                    <a href="/film/{{ $item->id }}" class="btn btn-primary">Detail</a>
                                @endguest
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
