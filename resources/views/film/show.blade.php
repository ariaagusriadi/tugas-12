@extends('layout.master')
@section('title')
    Halaman Film
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
           Detail Data Film
        </div>
        <div class="card-body">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{ asset('images/'.$film->poster) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{ $film->judul }}</h5>
                        <h5 class="card-title">{{ $film->tahun }}</h5>
                        <p class="card-text">{{ $film->ringkasan}}</p>
                        <a href="/film" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
        </div>
    </div>
@endsection
