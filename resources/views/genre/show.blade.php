@extends('layout.master')
@section('title')
    Halaman genre
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            genre
        </div>
        <div class="card-body">
            <div class="row">
                @forelse ($genre->film as $item)
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('images/' . $item->poster) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->judul }}</h5><br>
                                <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
                                <a href="/film/{{ $item->id }}" class="btn btn-primary">Detail</a>
                            </div>
                        </div>
                    </div>
                @empty
                    <h1>tidak ada film di genre ini</h1>
                @endforelse
            </div>

        </div>
    </div>
@endsection
