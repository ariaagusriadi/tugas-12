@extends('layout.master')
@section('title')
    Halaman genre
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            genre
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($genre as $key => $item)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="/genre/{{ $item->id }}" class="btn btn-sm btn-dark"><i class="fa fa-info"></i></a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>Data Cast Masih Kosong</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>
        </div>
    </div>
@endsection
