@extends('layout.master')
@section('title')
    Halaman Create Cast
@endsection
@section('content')
    <form action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="" class="control-label">Umur</label>
            <input type="number" class="form-control" name="umur" value="{{ $cast->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Bio</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                name="bio">{{ $cast->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <button class="btn btn-dark float-right"><i class="fa fa-save"></i> Simpan</button>
        </div>
    </form>
@endsection
