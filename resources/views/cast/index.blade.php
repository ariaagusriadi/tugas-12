@extends('layout.master')
@section('title')
    Halaman Cast
@endsection
@section('content')
    <a href="/cast/create" class="btn btn-dark float-right mb-4"><i class="fa fa-plus"></i> Tambah Cast</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="/cast/{{ $item->id }}" class="btn btn-sm btn-dark"><i class="fa fa-info"></i></a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-sm btn-warning"><i
                                    class="fa fa-edit"></i></a>
                            <form action="/cast/{{ $item->id }}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-sm btn-danger" onclick="confirm('are you sure delete?')"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Cast Masih Kosong</td>
                </tr>
            @endforelse

        </tbody>
    </table>
@endsection
