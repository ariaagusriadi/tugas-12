@extends('layout.master')
@section('title')
    Halaman Edit Profile
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Edit Data Profile
        </div>
        <div class="card-body">
            <form method="POST" action="/profile/{{ $profile->id }}">
                @csrf
                @method('put')
                <div class="form-group row">
                    <label for="Nama user" class="col-md-4 col-form-label text-md-right">Nama user</label>

                    <div class="col-md-6">
                        <input id="umur" type="text" class="form-control @error('umur') is-invalid @enderror" name="umur"
                            value="{{ $profile->user->name }}" disabled>

                        @error('umur')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Nama user" class="col-md-4 col-form-label text-md-right">email</label>

                    <div class="col-md-6">
                        <input id="umur" type="text" class="form-control @error('umur') is-invalid @enderror" name="umur"
                            value="{{ $profile->user->email }}" disabled>

                        @error('umur')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="umur" class="col-md-4 col-form-label text-md-right">Umur</label>

                    <div class="col-md-6">
                        <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror" name="umur"
                            value="{{ $profile->umur }}">

                        @error('umur')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                    <div class="col-md-6">

                        <textarea name="alamat" id="alamat" class="form-control  @error('alamat') is-invalid @enderror">
                            {{ $profile->alamat }}
                        </textarea>

                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="bio" class="col-md-4 col-form-label text-md-right">bio</label>

                    <div class="col-md-6">
                        <textarea name="bio" id="bio" class="form-control  @error('bio') is-invalid @enderror">
                            {{ $profile->bio }}
                        </textarea>

                        @error('bio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary float-right">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
