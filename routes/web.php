<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\ProfileController;
// use App\Http\Controllers\FilmController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@index');
Route::post('/welcome', 'AuthController@store');
Route::get('/table', function () {
    return view('page.table');
});
Route::get('/data-tables', function () {
    return view('page.datatable');
});


// crud Cast



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    // create
    // route unutk menuju tambah cast
    Route::get('/cast/create', 'CastController@create');

    // route unutk menyimpan data ke database
    Route::post('/cast', 'CastController@store');

    // read
    // route unutk menampikan data semua cast
    Route::get('/cast', 'CastController@index');

    // route unutk detail cast bedasarkan id
    Route::get('/cast/{cast_id}', 'CastController@show');

    // update
    // route unutk mengarah ke halaman form edit
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');

    // route unutk update data bedasarkan id
    Route::put('/cast/{cast_id}', 'CastController@update');

    // delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy');

    // profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('genre', 'genrecontroller');
});
// crud film
Route::resource('film', FilmController::class);
