<?php

namespace App\Http\Controllers;

use App\film;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;


class FilmController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth')->except('index', 'show',);
    }

    public function index()
    {
        $film = film::all();
        return view('film.index', compact('film'));
    }


    public function create()
    {
        return view('film.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'ringkasan' => 'required',
        ]);

        $postername = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('images'), $postername);

        $film = new film();
        $film->judul = request('judul');
        $film->tahun = request('tahun');
        $film->poster = $postername;
        $film->ringkasan = request('ringkasan');
        $film->save();

        return redirect('/film');
    }


    public function show($id)
    {
        $film = film::find($id);
        return view('film.show', compact('film'));
    }


    public function edit($id)
    {
        $film = film::find($id);
        return view('film.edit', compact('film'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'ringkasan' => 'required',
        ]);

        $film = film::find($id);
        if ($request->has('poster')) {
            $path = 'images/';
            File::delete($path . $film->poster);
            $postername = time() . '.' . $request->poster->extension();
            $request->poster->move(public_path('images'), $postername);
            $film->poster = $postername;
        }

        $film->judul = request('judul');
        $film->tahun = request('tahun');
        $film->ringkasan = request('ringkasan');
        $film->save();

        return redirect('/film');
    }


    public function destroy($id)
    {
        $film = film::find($id);
        $film->delete();

        $path = 'images/';
        File::delete($path. $film->poster);

        return redirect('/film');
    }
}
