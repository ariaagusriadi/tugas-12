<?php

namespace App\Http\Controllers;

use App\genre;
use Illuminate\Http\Request;

class genrecontroller extends Controller
{

    public function index()
    {
        $genre = genre::all();
        return view('genre.index' , compact('genre'));
    }


    public function show($id)
    {
        $genre = genre::find($id);
        return view('genre.show' , compact('genre'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
