<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\profile;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = Profile::where('user_id', Auth::id()) ->first();
        // dd($profile);

        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profile = profile::find($id);
        $profile->umur = $request['umur'];
        $profile->alamat = $request['alamat'];
        $profile->bio = $request['bio'];
        $profile->save();

        return redirect('/profile');
    }
}
