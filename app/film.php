<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = 'film';

    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre', 'genre_id'];

    public function genre()
    {
        return $this->belongsTo('App\genre', 'genre_id');
    }
}
